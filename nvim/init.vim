set nu
set termguicolors
com -bar W exe 'w !sudo tee >/dev/null %:p:S' | setl nomod

highlight TabLineFill ctermfg=LightGreen ctermbg=DarkGreen
highlight TabLine ctermfg=Blue ctermbg=Yellow
highlight TabLineSel ctermfg=Red ctermbg=Yellow


set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set foldmethod=syntax
set foldnestmax=10
set nofoldenable
set foldlevel=2
set completeopt-=preview
set guicursor=n-v-c:ver25,i-ci-ve:ver25,r-cr:hor20,o:hor50,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor,sm:block-blinkwait175-blinkoff150-blinkon175
let g:ycm_global_ycm_extra_conf = '/home/administrator/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py' 

let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
let g:ycm_confirm_extra_conf   = 0
let g:ale_lint_on_text_changed = 1
let g:ale_lint_on_save         = 1
let &t_EI = "\e[2 q"
let &t_SI = "\e[6 q"
let &t_ti = "\e[2 q"..&t_ti
let &t_te = "\e[2 q"..&t_te
let b:ale_linters                    = ['g++']
let g:ale_linters_explicit           = 1
let g:airline#extensions#ale#enabled = 1
let g:ale_cursor_detail              = 0

let g:buffet_powerline_separators = 1
let g:buffet_tab_icon = "\uf00a"
let g:buffet_left_trunc_icon = "\uf0a8"
let g:buffet_right_trunc_icon = "\uf0a9"

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'


let &t_SI = "\033[5 q"
let &t_EI = "\033[5 q"

nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-u> :NERDTree<CR>
nnoremap <C-i> :NERDTreeToggle<CR>
nnoremap <C-o> :NERDTreeFind<CR>

noremap <Tab> :bn<CR>
noremap <S-Tab> :bp<CR>
noremap <Leader><Tab> :Bw<CR>
noremap <Leader><S-Tab> :Bw!<CR>
noremap <C-t> :tabnew split<CR>
noremap <Leader>y "*y
noremap <Leader>p "*p
noremap <Leader>Y "+y
noremap <Leader>P "+p

"nmap <leader>1 <Plug>BuffetSwitch(1)
"nmap <leader>2 <Plug>BuffetSwitch(2)
"nmap <leader>3 <Plug>BuffetSwitch(3)
"nmap <leader>4 <Plug>BuffetSwitch(4)
"nmap <leader>5 <Plug>BuffetSwitch(5)
"nmap <leader>6 <Plug>BuffetSwitch(6)
"nmap <leader>7 <Plug>BuffetSwitch(7)
"nmap <leader>8 <Plug>BuffetSwitch(8)
"nmap <leader>9 <Plug>BuffetSwitch(9)
"nmap <leader>0 <Plug>BuffetSwitch(10)




call plug#begin('~/.vim/plugged')
Plug 'ActivityWatch/aw-watcher-vim'
Plug 'octol/vim-cpp-enhanced-highlight'
Plug 'airblade/vim-gitgutter'
Plug 'vim-airline/vim-airline'
Plug 'fatih/vim-go'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'xuyuanp/nerdtree-git-plugin'
Plug 'scrooloose/nerdcommenter'
Plug 'mg979/vim-visual-multi'
Plug 'yggdroot/indentline'
Plug 'sirver/ultisnips'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/fzf'
Plug 'rdnetto/YCM-Generator'
Plug 'ycm-core/YouCompleteMe'
Plug 'dense-analysis/ale'
Plug 'junegunn/fzf.vim'
Plug 'amiorin/vim-project'
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
call plug#end()
